function Card(suit, name, value) {
    this.suit = suit;
    this.name = name;
    this.value = (value < 10) ? value : 10;
    this.getDetail = function () {
        return (this.value  + "\t" + this.name + "\t" + this.suit);
    }
}

function Deck() {
    this.suitStrArr = ["Diamonds", "Clubs", "Hearts", "Spades"];
    this.nameStrArr = ["Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"];
    this.deck = [];

    this.getDeck = function() {
        return this.deck;
    }

    this.shuffle = function() {
        for (var count=1; count<=1000; count++) {
            var pos1 = Math.floor(Math.random() * this.deck.length);
            var pos2 = Math.floor(Math.random() * this.deck.length);

            var tempCard = this.deck[pos1];
            this.deck[pos1] = this.deck[pos2];
            this.deck[pos2] = tempCard;
        }
    } 

    for (var s=0; s<this.suitStrArr.length; s++) {
        for (var n=0; n<this.nameStrArr.length; n++) {
            var card = new Card(this.suitStrArr[s], this.nameStrArr[n], (n+1));
            this.deck.push(card);
        }
    }
}

function deckOfCards() {
    var d = new Deck();
    d.shuffle();
    return d.getDeck();
}

var taitee = deckOfCards();

taitee.forEach(function (card) {
    console.log(card.getDetail());
});